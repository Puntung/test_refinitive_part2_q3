const express = require('express')
const app = express()
const port = 3000
const puppeteer = require('puppeteer')
const cheerio = require('cheerio');

const findFundWithSymbol = (data,fundCode) => {
    const $ = cheerio.load(data)
    var table = $('tbody')
    table[0].children.shift()
    var allFund = table[0].children
    var clearBlank =  allFund.filter((item,index) => {
        if(item.children[0].type == "text"){
            item.children.shift()
            return item
        }
        else{
            return item
        }
    })
    // console.log(clearBlank)
    var findFundCode = clearBlank.find((item,index) => item.children[0].children[0].data.trim() == fundCode)
    if(findFundCode){
        return findFundCode.children[1].children[0].data
    }
    else{
        return "Can't find data from this fund code."
    }
}

const scrapingWebFundData = () => {
    const cookies = [{"hasCookie":true}]
    return new Promise(async (resolve, reject) => {
        const browser = await puppeteer.launch()
        const page = await browser.newPage()
        await page.goto('https://codequiz.azurewebsites.net/')
        await page.click("input[value=Accept]");
        await page.waitForSelector("table");
        const content = await page.evaluate(() => document.body.innerHTML)
        resolve(content)
        await browser.close()
    })

}

app.listen(port, async () => {
    const fundCodeFromCmd = process.argv[2]
    const result = await scrapingWebFundData()
    const findFund = findFundWithSymbol(result,fundCodeFromCmd) 
    console.log(findFund)
})